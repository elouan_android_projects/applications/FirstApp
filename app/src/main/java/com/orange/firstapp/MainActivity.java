package com.orange.firstapp;

import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.orange.FirstApp.R;


public class MainActivity extends AppCompatActivity {


    private final String TAG = getClass().getSimpleName();

    protected static final String EXTRA_MESSAGE = "useful?";
    protected EditText editText;
    protected TextView textView;
    protected String message;
    protected boolean notEmpty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.Main_EditText);
        textView = findViewById(R.id.Main_WarningText);


        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                message = editText.getText().toString();
                notEmpty = false;
                textView.setText("");
                if (message.length() > 0) {
                    int i = 0;
                    while ((i < message.length()) && (!notEmpty)) {
                        if (message.charAt(i) != ' ') {
                            notEmpty = true;
                        }
                        i++;
                    }
                    if (!notEmpty) {
                        textView.setText(R.string.warning_emptystring);
                    }
                }
            }
        });
    }

    public void textErrorDialog() {
        DialogFragment newFragment = new NullStringDialog();
        String errorMessage = new String();

        Bundle errorMsg = new Bundle();
        if (message.equals("")) {
            errorMessage = getString(R.string.warning_nullStringTitle);
        } else if (!notEmpty) {
            errorMessage = getString(R.string.warning_emptystring);
        }
        errorMsg.putString("errorMessage", errorMessage);
        newFragment.setArguments(errorMsg);

        newFragment.show(getSupportFragmentManager(), "errorMessage");
    }

    // Called when the user taps the Send button
    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        message = editText.getText().toString();

        if ((message.equals("")) || !(notEmpty)) {
            //Error in text typed
            this.textErrorDialog();
        } else {
            //Text typed is ok -> delete useless spaces
            boolean firstCharFound = false, lastCharFound = false;
            int firstCharPos = 0, lastCharPos = 0;
            int i = 0;
            while ((i <= message.length() - 1) && (!firstCharFound)) {
                if (message.charAt(i) != ' ') {
                    firstCharFound = true;
                    firstCharPos = i;
                }
                i++;
            }
            i = message.length() - 1;
            while ((i >= 0) && (!lastCharFound)) {
                if (message.charAt(i) != ' ') {
                    lastCharFound = true;
                    lastCharPos = i;
                }
                i--;
            }
            StringBuilder tmpText = new StringBuilder();
            for (int j = firstCharPos; j <= lastCharPos; j++) {
                tmpText.append(message.charAt(j));
            }
            //Send text
            message = tmpText.toString();
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
        }
    }

}
