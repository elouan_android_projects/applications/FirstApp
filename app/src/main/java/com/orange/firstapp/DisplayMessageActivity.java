package com.orange.firstapp;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.orange.FirstApp.R;

public class DisplayMessageActivity extends AppCompatActivity {

    static float textSize;
    private final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.displayMessage_DispalyedText);
        textView.setText(message);

        //get the text size
        textSize = textView.getTextSize();
        Log.d(TAG, "false size : " + textSize + "     " + textView.getTextScaleX());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar, menu);
        return true;
    }

    public void showeditColor() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        EditColorDialog newFragment = new EditColorDialog();
        // Show the fragment fullscreen
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment)
                .addToBackStack(null).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        TextView textView = findViewById(R.id.displayMessage_DispalyedText);
        switch (item.getItemId()) {
            // User chose the color of the text
            case R.id.action_colorChange:
                showeditColor();
                // overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
               /* if (textView.getTextColors().getDefaultColor() == Color.parseColor("#ff9000")) {
                    textView.setTextColor(Color.parseColor("#444444"));
                } else {
                    textView.setTextColor(Color.parseColor("#ff9000"));
                }*/
                return true;
            // User chose the color of the text
            case R.id.action_fontSizeChange:
                if (textView.getTextSize() == textSize) {
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize + textSize / 2);
                } else {
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                }
                return true;
            default:
                // user's action was not recognized.
                return super.onOptionsItemSelected(item);

        }
    }
}
