package com.orange.firstapp;

import android.app.AlertDialog;
import android.app.Dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.orange.FirstApp.R;

public class NullStringDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //set title and text
        builder.setTitle(R.string.dialog_Error);
        String errorMessage = getArguments().getString("errorMessage");
        builder.setMessage(errorMessage);

        // Add the buttons
        builder.setPositiveButton(R.string.ok_1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        return builder.create();
    }

}